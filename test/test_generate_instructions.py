#!/usr/bin/python
import sys
import unittest
import generate_instructions


class TestGenerateInstructions(unittest.TestCase):

    def test_load_config(self):
        print("running test_load_config")
        with self.assertRaises(SystemExit):
            generate_instructions.load_config("wrongPath")

    def test_get_config(self):
        print("running test_get_config")
        generate_instructions.json_env_configs = [{
                    "ENV": "PPE",
                    "CONFIG": {
                        "ADMIN_SERVER_PORT": "7001"
                    }
                }]
        config = generate_instructions.get_config("PPE")
        self.assertEqual(config['ADMIN_SERVER_PORT'], "7001")

if __name__ == '__main__':
    unittest.main()
