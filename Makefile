install:
	##MACOSX
	##sudo easy_install pip

	sudo pip install pystache
	sudo pip install xhtml2pdf
	sudo pip install html5lib==1.0b8
	##sudo pip install mock --ignore-installed six

test: unit-test

unit-test:
	python -m unittest discover -s test

run:
	./generate_all_instructions.sh
