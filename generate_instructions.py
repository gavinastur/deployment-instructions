#!/usr/bin/python
import sys
import os.path
import json
import pystache
import zipfile
import time
import glob
from xhtml2pdf import pisa

OUTPUT_ROOT = "./output/"
TEMPLATE_ROOT = "./templates/"

def load_config(path):
    if os.path.isfile(path):
        try:
            tmp_json_configs = json.loads(open(path).read())
        except Exception as e:
            print("The config json could not parsed: " + str(e))
    else:
        print("Could not read config file: " + path)
        sys.exit()
    return tmp_json_configs

def generate_and_convert_instructions(environment, template_paths):
    env_config = get_config(environment)
    for path in template_paths:
        template = render_template(path, env_config)
        output_path = create_output_path(path,environment)
        convert_html_to_pdf(output_path, template)

def get_config(environment):
    for env_config in json_env_configs:
        if env_config['ENV'] == environment:
            return env_config

def render_template(path, environment_config):
    renderer = pystache.Renderer(missing_tags='strict')
    return renderer.render_path(path, environment_config)

def create_output_path(path,environment):
    if not os.path.exists(OUTPUT_ROOT + environment):
        os.makedirs(OUTPUT_ROOT + environment)
    return path.replace(".mustache", "-" + environment + ".pdf").replace(TEMPLATE_ROOT, OUTPUT_ROOT + environment + "/")

def convert_html_to_pdf(path, source_html):
    resultFile = open(path, "w+b")
    pisa.CreatePDF(source_html,dest=resultFile)
    resultFile.close()

def zip_documents():
    archive = zipfile.ZipFile('docs-' + time.strftime("%Y%m%d-%H%M%S") + '.zip', 'w', zipfile.ZIP_DEFLATED)
    print OUTPUT_ROOT
    for root, dirs, files in os.walk(OUTPUT_ROOT):
        for file in files:
            archive.write(os.path.join(root, file))
    archive.close()


def main():
    if len(sys.argv) >= 2:

        if len(sys.argv) > 2:
            environment = sys.argv[2]
        else:
            environment = "all"
    else:
        print("----------------------")
        print("generate_instructions.py")
        print("----------------------")
        print("Usage instructions: ")
        print("python generate_instructions.py [Component] [Environment]")
        print("----------------------")
        sys.exit()

    print("Generating instructions for Component: {0}, Environment(s): {1}".format(sys.argv[1],environment))

    global json_env_configs
    json_env_configs = load_config("./config/iwf_config.json")

    template_paths = glob.glob(TEMPLATE_ROOT + "*.mustache")

    if environment == "all":
        for env_config in json_env_configs:
            generate_and_convert_instructions(env_config['ENV'], template_paths)

    else:
        generate_and_convert_instructions(environment, template_paths)

    #zip_documents()

    print("Done")

if __name__ == '__main__':
    main()
