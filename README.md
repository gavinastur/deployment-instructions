# Python templating of deployment instructions #
======================================

**Libraries:**

pystache  
xhtml2pdf  
html5lib  


If we want to have the html in a file as well. Add this function to the code and call before converting to pdf.

```
#!python

def write_output(template):
    output_file = open(get_output_path(), "w")
    output_file.write(template)
    output_file.close()
```

Remover - removes all CSS comments from within the <style/> block.


**TODOs**  
 - add validation    
 - add more tests  


## Install, running tests and file generating
```
make install
```

**Test:**
```
make test
```

**Run:**
```
make run

```